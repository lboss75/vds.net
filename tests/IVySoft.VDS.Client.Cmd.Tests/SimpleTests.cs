using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace IVySoft.VDS.Client.Cmd.Tests
{
    public class SimpleTests : VDSTest
    {
        private const string login = "test@test.ru";
        private const string password = "123qwe";

        public SimpleTests(ITestOutputHelper output)
            : base(output)
        {
        }

        [Fact]
        public void SyncFilesTest()
        {
            if (Directory.Exists(VdsProcess.RootFolder))
            {
                this.WriteLine($"Delete folder {VdsProcess.RootFolder}");
                Directory.Delete(VdsProcess.RootFolder, true);
            }

            this.WriteLine($"Init root in {VdsProcess.RootFolder}");
            Assert.Equal(0, VdsProcess.InitRoot(login, password));

            using (var servers = new VdsProcessSet(3))
            {
                this.WriteLine($"Starting servers");
                servers.start();
                System.Threading.Thread.Sleep(10000);
                this.WriteLine($"Waiting sync");
                servers.waiting_sync();

                //servers.create_user(4, login, password);

                this.WriteLine($"Allocate storage");
                servers.allocate_storage(login, password, "1G");
                this.WriteLine($"Waiting sync");
                servers.waiting_sync();

                var source_folder = Path.Combine(VdsProcess.RootFolder, "Original");
                var dest_folder_local = Path.Combine(VdsProcess.RootFolder, "DestinationLocal");
                var dest_folder_remote = Path.Combine(VdsProcess.RootFolder, "DestinationRemote");

                Directory.CreateDirectory(source_folder);
                Directory.CreateDirectory(dest_folder_local);
                Directory.CreateDirectory(dest_folder_remote);

                this.WriteLine($"Generate random files");
                const int file_count = 200;
                var rnd = new Random();
                for (int i = 0; i < file_count / 4; ++i)
                {
                    GenerateRandomFile(rnd, Path.Combine(dest_folder_local, i.ToString()));
                }
                for (int i = file_count / 4; i < file_count / 2; ++i)
                {
                    File.Copy(
                        Path.Combine(dest_folder_local, rnd.Next(0, file_count / 4).ToString()),
                        Path.Combine(dest_folder_local, i.ToString()));
                }
                for (int i = file_count / 2; i < 3 * file_count / 4; ++i)
                {
                    GenerateRandomFile(rnd, Path.Combine(dest_folder_remote, i.ToString()));
                }
                for (int i = 3 * file_count / 4; i < file_count; ++i)
                {
                    File.Copy(
                        Path.Combine(dest_folder_local, rnd.Next(0, file_count / 2).ToString()),
                        Path.Combine(dest_folder_remote, i.ToString()));
                }

                this.WriteLine($"Create channel");
                Assert.Equal(0 , servers.create_channel(1, login, password, IVySoft.VDS.Client.Api.ChannelTypes.file_channel, "test"));
                this.WriteLine($"Waiting sync");
                servers.waiting_sync();

                string channel_id = null;
                this.WriteLine($"Looking channels");
                foreach (var channel in servers.GetChannels(login, password, 1))
                {
                    channel_id = channel.Id;
                    if (!string.IsNullOrEmpty(channel_id))
                    {
                        this.WriteLine($"Found channel {channel_id}");
                        break;
                    }
                }

                Assert.True(!string.IsNullOrEmpty(channel_id));

                this.WriteLine($"Sync local files");
                servers.sync_files(login, password, channel_id, 1, dest_folder_local);
                servers.sync_files(login, password, channel_id, 2, dest_folder_remote);

                this.WriteLine($"Waiting sync");
                servers.waiting_sync();

                this.WriteLine($"Sync remote files");
                servers.sync_files(login, password, channel_id, 0, source_folder);
                this.WriteLine($"Compare files");
                for (int i = 0; i < file_count / 2; ++i)
                {
                    CompareFile(Path.Combine(source_folder, i.ToString()), Path.Combine(dest_folder_local, i.ToString()));
                }
                for (int i = file_count / 2; i < file_count; ++i)
                {
                    CompareFile(Path.Combine(source_folder, i.ToString()), Path.Combine(dest_folder_remote, i.ToString()));
                }

            }
        }
    }
}
