﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Xunit;
using Xunit.Abstractions;

namespace IVySoft.VDS.Client.Cmd.Tests
{
    public class DistributionTest : VDSTest
    {
        private const string login = "test@test.ru";
        private const string password = "123qwe";

        public DistributionTest(ITestOutputHelper output)
            : base(output)
        {
        }

        /// <summary>
        /// All 16 server must have replica of data
        /// </summary>
        [Fact]
        public void SimpleTest()
        {
            if (Directory.Exists(VdsProcess.RootFolder))
            {
                this.WriteLine($"Delete folder {VdsProcess.RootFolder}");
                Directory.Delete(VdsProcess.RootFolder, true);
            }

            this.WriteLine($"Init root in {VdsProcess.RootFolder}");
            Assert.Equal(0, VdsProcess.InitRoot(login, password));

            using (var servers = new VdsProcessSet(8))
            {
                this.WriteLine($"Starting servers");
                servers.start();
                System.Threading.Thread.Sleep(10000);
                this.WriteLine($"Waiting sync");
                servers.waiting_sync();

                this.WriteLine($"Allocate storage");
                servers.allocate_storage(login, password, "1G");
                this.WriteLine($"Waiting sync");
                servers.waiting_sync();

                var source_folder = Path.Combine(VdsProcess.RootFolder, "Original");

                Directory.CreateDirectory(source_folder);

                this.WriteLine($"Generate random file");
                var rnd = new Random();
                GenerateRandomFile(rnd, Path.Combine(source_folder, "test"));

                this.WriteLine($"Create channel");
                Assert.Equal(0, servers.create_channel(1, login, password, IVySoft.VDS.Client.Api.ChannelTypes.file_channel, "test"));

                string channel_id = null;
                this.WriteLine($"Looking channels");
                foreach (var channel in servers.GetChannels(login, password, 1))
                {
                    channel_id = channel.Id;
                    if (!string.IsNullOrEmpty(channel_id))
                    {
                        this.WriteLine($"Found channel {channel_id}");
                        break;
                    }
                }

                Assert.True(!string.IsNullOrEmpty(channel_id));

                this.WriteLine($"Sync local files");
                servers.sync_files(login, password, channel_id, 1, source_folder);

                this.WriteLine($"Waiting sync");
                servers.waiting_sync();

                for(int try_count = 0; try_count < 1000; ++try_count)
                {
                    var distribution = servers.GetDistribution(login, password, 1, channel_id, "test");
                    bool is_OK = true;
                    foreach (var version in distribution)
                    {
                        foreach (var block in version.DistributionMap)
                        {
                            var set = new SortedSet<string>();
                            int count = 0;
                            foreach (var replica in block.replicas)
                            {
                                foreach (var node in replica.nodes)
                                {
                                    if(!set.Contains(node))
                                    {
                                        set.Add(node);
                                        if (++count >= servers.Count)
                                        {
                                            break;
                                        }
                                    }
                                }

                                if(count < servers.Count)
                                {
                                    is_OK = false;
                                    break;
                                }
                            }
                            if (!is_OK)
                            {
                                break;
                            }
                        }
                        if (!is_OK)
                        {
                            break;
                        }
                    }

                    if (is_OK)
                    {
                        break;
                    }

                    this.WriteLine($"Waiting sync");
                    servers.waiting_sync();

                    Thread.Sleep(TimeSpan.FromSeconds(30));
                }
            }
        }
    }
}
