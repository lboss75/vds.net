﻿using IVySoft.VDS.Client.Api;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IVySoft.VDS.Client.UI.Logic.Files
{
    public class FileListState : INotifyPropertyChanged
    {
        private readonly ObservableCollection<VdsChannelFileListSource> sources_ = new ObservableCollection<VdsChannelFileListSource>();
        private VdsChannelFileListSource fileListSource_;

        public FileListState()
        {
        }

        public ObservableCollection<VdsChannelFileListSource> Sources { get => this.sources_; }
        public VdsChannelFileListSource CurrentSource
        {
            get => this.fileListSource_;
            set
            {
                if(this.fileListSource_ != value)
                {
                    this.fileListSource_ = value;

                    if(this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs(nameof(CurrentSource)));
                        this.PropertyChanged(this, new PropertyChangedEventArgs(nameof(Files)));
                    }
                }
            }
         }

        public async Task Refresh(CancellationToken token, Action<Action> dispatchAction)
        {
            if(null != this.fileListSource_)
            {
                await this.fileListSource_.Refresh(token, dispatchAction);
            }
        }

        public ObservableCollection<Logic.Model.ChannelMessageFileInfo> Files { get => this.fileListSource_?.Files; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
