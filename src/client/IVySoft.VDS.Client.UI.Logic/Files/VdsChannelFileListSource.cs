﻿using IVySoft.VDS.Client.Api;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace IVySoft.VDS.Client.UI.Logic.Files
{
    public class VdsChannelFileListSource
    {
        public Channel Channel { get { return this.channel_; } }

        private readonly ThisUser user_;
        private readonly Channel channel_;
        private readonly ObservableCollection<Logic.Model.ChannelMessageFileInfo> files_ = new ObservableCollection<Logic.Model.ChannelMessageFileInfo>();

        public VdsChannelFileListSource(ThisUser user, Channel channel)
        {
            this.user_ = user;
            this.channel_ = channel;
        }

        internal ObservableCollection<Logic.Model.ChannelMessageFileInfo> Files
        {
            get
            {
                return this.files_;
            }
        }

        public async System.Threading.Tasks.Task Refresh(System.Threading.CancellationToken token, Action<Action> dispatchAction)
        {
            using (var s = new VdsService())
            {
                foreach(var message in await s.Api.GetChannelMessages(token, this.channel_))
                {
                    foreach(var f in message.Files)
                    {
                        this.AddFile(f, dispatchAction);
                    }
                }
            }
        }

        private void AddFile(ChannelMessageFileInfo f, Action<Action> dispatchAction)
        {
            foreach(var file in this.files_)
            {
                if(file.Name == f.Name)
                {
                    file.Merge(f);
                    return;
                }
            }

            dispatchAction(() =>
            {
                var item = new Model.ChannelMessageFileInfo(f);

                string file_path;
                if (VdsService.DownloadCache.TryGetFile(f.Id, out file_path))
                {
                    item.Progress = 100;
                }

                this.files_.Add(item);
            });
        }

        public override string ToString()
        {
            return this.channel_.Name;
        }
    }
}
