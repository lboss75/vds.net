﻿using IVySoft.VDS.Client.UI.Logic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IVySoft.VDS.Client.UI.WPF.Common
{
    /// <summary>
    /// Interaction logic for UploadWindow.xaml
    /// </summary>
    public partial class UploadWindow : Window
    {
        public string[] Files { get; set; }

        public new ObservableCollection<UI.Logic.Model.ChannelMessageFileInfo> DataContext
        {
            get
            {
                return (ObservableCollection<UI.Logic.Model.ChannelMessageFileInfo>)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

        public Api.Channel Channel { get; set; }

        public UploadWindow()
        {
            InitializeComponent();
            this.DataContext = new ObservableCollection<Logic.Model.ChannelMessageFileInfo>();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var files = new List<FileUploadStream>();
            foreach (var f in this.Files)
            {
                var item = new Logic.Model.ChannelMessageFileInfo(
                        System.IO.Path.GetFileName(f),
                        new System.IO.FileInfo(f).Length);
                item.InProgress = true;

                this.DataContext.Add(item);

                files.Add(new FileUploadStream
                {
                    Name = System.IO.Path.GetFileName(f),
                    SystemPath = f,
                    ProgressCallback = (x =>
                    {
                        item.Progress = x;
                        return true;
                    }),
                    UploadedCallback = (x =>
                    {
                        item.Info = x;
                        item.InProgress = false;
                    })
                });
            }
            ProgressWindow.Run(
                "File upload",
                Window.GetWindow(this), async token =>
                {
                    using (var s = new VdsService())
                    {
                        try
                        {
                            await s.Api.UploadFiles(
                                token,
                                this.Channel,
                                "Upload files",
                                files.ToArray());
                        }
                        catch (Exception ex)
                        {
                            Dispatcher.Invoke(() => {
                                MessageBox.Show(UIUtils.GetErrorMessage(ex), "Отправка сообщения", MessageBoxButton.OK, MessageBoxImage.Error);
                            });
                        }
                    }
                });

        }
    }
}
