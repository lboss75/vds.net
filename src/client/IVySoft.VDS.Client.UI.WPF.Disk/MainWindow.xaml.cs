﻿using IVySoft.VDS.Client.Api;
using IVySoft.VDS.Client.UI.Logic;
using IVySoft.VDS.Client.UI.Logic.Files;
using IVySoft.VDS.Client.UI.WPF.Common;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IVySoft.VDS.Client.UI.WPF.Disk
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ThisUser user_;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new FileListState();
        }

        public new FileListState DataContext
        {
            get
            {
                return (FileListState)base.DataContext;
            }
            set
            {
                base.DataContext = value;
            }
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Login();
        }

        private void Login()
        {
            var dlg = new LoginWindow();
            dlg.Owner = this;
            if (dlg.ShowDialog() != true)
            {
                this.Close();
                return;
            }
            ProgressWindow.Run(
                "User login",
                this, async token =>
            {
                using (var s = new VdsService())
                {
                    try
                    {
                        this.user_ = await s.Api.Login(token, dlg.Login, dlg.Password);
                        this.OnGetChannels(await s.Api.GetChannels(token, this.user_));
                    }
                    catch (Exception ex)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            MessageBox.Show(this, UIUtils.GetErrorMessage(ex), this.Title, MessageBoxButton.OK, MessageBoxImage.Error);
                            this.Login();
                        });
                    }
                }
            });
        }

        private void OnGetChannels(Api.Channel[] result)
        {
            this.Dispatcher.Invoke(() =>
            {
                foreach (var channel in result)
                {
                    this.DataContext.Sources.Add(new VdsChannelFileListSource(this.user_, channel));
                }
            });
        }
        private void FileListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.DownloadMenuItem_Click(sender, e);
        }

        private void CurrentSource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.Refresh();
        }
        private void RefreshBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Refresh();
        }

        private void Refresh()
        {
            var context = this.DataContext;
            ProgressWindow.Run("Update " + context.CurrentSource?.ToString(), Window.GetWindow(this), token => context.Refresh(token, x => Dispatcher.Invoke(x)));
        }
        private void PropertiesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (1 == FileListView.SelectedItems.Count)
            {
                DistributionMapWindow wnd = new DistributionMapWindow();
                var item = (UI.Logic.Model.ChannelMessageFileInfo)FileListView.SelectedItems[0];
                wnd.DataContext = item.Info;
                wnd.Show();
            }
        }

        private void DownloadMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (1 == FileListView.SelectedItems.Count)
            {
                var fb = (UI.Logic.Model.ChannelMessageFileInfo)FileListView.SelectedItems[0];

                string file_path;
                if (VdsService.DownloadCache.TryGetFile(fb.Info.Id, out file_path))
                {
                    new System.Diagnostics.Process
                    {
                        StartInfo = new System.Diagnostics.ProcessStartInfo(file_path)
                        {
                            UseShellExecute = true
                        }
                    }.Start();
                    return;
                }

                ProgressWindow.Run(
                    "File download",
                    Window.GetWindow(this), async token =>
                    {
                        using (var s = new VdsService())
                        {
                            try
                            {
                                var f = await s.Download(token, fb.Info, x => true);
                                new System.Diagnostics.Process
                                {
                                    StartInfo = new System.Diagnostics.ProcessStartInfo(f)
                                    {
                                        UseShellExecute = true
                                    }
                                }.Start();
                            }
                            catch (Exception ex)
                            {
                                Dispatcher.Invoke(() =>
                                {
                                    MessageBox.Show(UIUtils.GetErrorMessage(ex), "Ошибка скачивания файла", MessageBoxButton.OK, MessageBoxImage.Error);
                                });
                            }
                        }
                    });
            }

        }

        private void Upload_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.Title = "Select files to update";
            if (dlg.ShowDialog() == true)
            {
                var uploadWnd = new UploadWindow();
                uploadWnd.Owner = this;
                uploadWnd.Channel = this.DataContext.CurrentSource.Channel;
                uploadWnd.Files = dlg.FileNames;
                uploadWnd.Show();
            }
        }
    }
}
