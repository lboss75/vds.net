﻿using CommandLine;

namespace IVySoft.VDS.Client.Cmd
{
    [Verb("distribution", HelpText = "Get distribution of file")]
    public class DistributionOptions : BaseOptions
    {
        [Option('i', "id", Required = true, HelpText = "Channel id")]
        public string ChannelId { get; set; }

        [Option('n', "file-name", Required = true, HelpText = "File name")]
        public string FileName { get; set; }
    }
}
