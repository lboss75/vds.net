﻿using IVySoft.VDS.Client.Api;

namespace IVySoft.VDS.Client.Cmd
{
    public class DistributionVesion
    {
        public DistributionVesion(ChannelMessageFileInfo version, SyncStatistic[] dist_map)
        {
            this.Version = version;
            this.DistributionMap = dist_map;
        }

        public ChannelMessageFileInfo Version { get; }
        public SyncStatistic[] DistributionMap { get; }
    }
}