using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using Newtonsoft.Json;
using System.IO;

namespace IVySoft.VDS.Client.AutoUpdate
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                var process = Process.GetCurrentProcess();
                string fullPath = process.MainModule.FileName + ".json";

                using (var file = File.OpenText(fullPath))
                {
                    var serializer = new JsonSerializer();
                    var current = (CurrentVersionJson)serializer.Deserialize(file, typeof(CurrentVersionJson));
                    return AutoUpdateCheck.Process(current.channel_name, new Version(current.version)).Result ? 0 : 2;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 1;
            }
        }
    }
}
