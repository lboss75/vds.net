﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IVySoft.VDS.Client.AutoUpdate
{
    public class CurrentVersionJson
    {
        public string channel_name { get; set; }
        public string version { get; set; }
    }
}
