﻿using IVySoft.VDS.Client.UI.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IVySoft.VDS.Client.AutoUpdate
{
    class AutoUpdateCheck
    {
        private const string autoupdate_login = "autoupdate";
        private const string autoupdate_password = "autoupdate";

        public static async Task<bool> Process(string channel_name, Version version)
        {
            using(var token = new CancellationTokenSource(TimeSpan.FromMinutes(10))) { 
            using (var s = new VdsService())
            {
                    var user = await s.Api.Login(token.Token, autoupdate_login, autoupdate_password);
                    var channel = (await s.Api.GetChannels(token.Token, user)).Single(x => x.Name == channel_name);
                    foreach (var item in await s.Api.GetChannelMessages(token.Token, channel))
                    {
                        var v = new Version(item.Message);
                        if (v > version)
                        {
                            foreach (var f in item.Files)
                            {
                                string file_path;
                                if (!VdsService.DownloadCache.TryGetFile(f.Id, out file_path))
                                {
                                    file_path = await s.Download(token.Token, f, x => true);
                                }

                                var p = new System.Diagnostics.Process
                                {
                                    StartInfo = new System.Diagnostics.ProcessStartInfo(file_path)
                                    {
                                        UseShellExecute = true
                                    }
                                };
                                p.Start();
                                return p.WaitForExit(1000 * 60 * 10) && 0 == p.ExitCode;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            return false;
        }
    }
}
