﻿using IVySoft.VDS.Client.Api;
using IVySoft.VDS.Client.UI.Logic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Documents;

namespace IVySoft.VDS.Client.UI.WPF.Monitor
{
    public class ServerStatisticRow : INotifyPropertyChanged
    {
        private readonly string service_uri;
        private readonly string node_id_;
        private string log_head_;
        private int db_queue_length_;
        private int send_queue_size_;
        private int sent_bypes_;

        public event PropertyChangedEventHandler PropertyChanged;

        public string ServiceUri { get => service_uri; }
        public string NodeId { get => node_id_; }
        public string LogHead { get => log_head_; }
        public int DBQueueLength { get => db_queue_length_; }
        public int SendQueueSize { get => send_queue_size_; }
        public int SentBypes { get => sent_bypes_; }

        public ObservableCollection<ServerSessionStatistic> Sessions { get; } = new ObservableCollection<ServerSessionStatistic>();
        public ObservableCollection<ServerStatisticQuota> Quotas { get; } = new ObservableCollection<ServerStatisticQuota>();
        public ObservableCollection<ServerRouteStatistic> Route { get; } = new ObservableCollection<ServerRouteStatistic>();

        public ServerStatisticRow(string service_uri, ServerStatistic stat)
        {
            this.service_uri = service_uri;
            this.node_id_ = stat.route.node_id;
            
            foreach(var session in stat.session.items)
            {
                this.Sessions.Add(new ServerSessionStatistic(stat.route.node_id, session));
            }
            foreach (var route in stat.route.items)
            {
                this.Route.Add(new ServerRouteStatistic(route));
            }
        }

        internal void Update(ServerStatistic stat)
        {
            Logic.CollectionUtils.Update(
                this.Sessions,
                stat.session.items,
                (x, y) => x.Partner == y.partner,
                (x, y) => x.Update(stat.route.node_id, y),
                x => new ServerSessionStatistic(stat.route.node_id, x));

            Logic.CollectionUtils.Update(
                this.Quotas,
                stat.session.quota,
                (x, y) => x.Address == y.address,
                (x, y) => x.Update(y),
                x => new ServerStatisticQuota(x));

            Logic.CollectionUtils.Update(
                this.Route,
                stat.route.items,
                (x, y) => x.NodeId == y.node_id && x.Proxy == y.proxy,
                (x, y) => x.Update(y),
                x => new ServerRouteStatistic(x));

            this.db_queue_length_ = stat.db_queue_length;
            this.send_queue_size_ = stat.session.send_queue_size;
            this.sent_bypes_ = stat.session.sent_bypes;

            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(DBQueueLength)));
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SendQueueSize)));
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SentBypes)));
        }

        internal void UpdateLogHead(string log_head)
        {
            if (this.log_head_ != log_head)
            {
                this.log_head_ = log_head;
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(LogHead)));
            }
        }
    }
}